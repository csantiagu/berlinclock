package com.cas.pojo;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * User: santiaguc
 */
public class BerlinClockImplTest extends BerlinClockImpl{

    @Test
    public void testGetBerlinClockString(){
        assertNull(getBerlinClockString(null));
        assertEquals("Y OOOO OOOO OOOOOOOOOOO OOOO",getBerlinClockString("00:00:00"));
        assertEquals("O RROO RRRO YYROOOOOOOO YYOO",getBerlinClockString("13:17:01"));
        assertEquals("O RRRR RRRO YYRYYRYYRYY YYYY",getBerlinClockString("23:59:59"));
        assertEquals("Y RRRR RRRR OOOOOOOOOOO OOOO",getBerlinClockString("24:00:00"));
    }
}
