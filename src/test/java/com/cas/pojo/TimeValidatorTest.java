package com.cas.pojo;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * User: santiaguc
 */
public class TimeValidatorTest {

    TimeValidator timeValidator = new TimeValidator();

    @Test
    public void testValidateTime(){
        assertFalse(timeValidator.validateTime(null));
        assertTrue(timeValidator.validateTime("20:20:20"));
        assertFalse(timeValidator.validateTime("20:2:20"));
        assertFalse(timeValidator.validateTime("2:2:2"));
        assertFalse(timeValidator.validateTime("20:2:2"));
        assertFalse(timeValidator.validateTime("120:112:120"));
        assertFalse(timeValidator.validateTime("40:40:10"));
        assertFalse(timeValidator.validateTime("AA:10:BB"));

    }

    @Test
    public void testExceptionCase(){
        assertFalse(timeValidator.exceptionalCase(null));
        assertTrue(timeValidator.exceptionalCase("24:00:00"));
        assertFalse(timeValidator.exceptionalCase("25:00:00"));
        assertFalse(timeValidator.exceptionalCase("23:05:01"));
    }


}
