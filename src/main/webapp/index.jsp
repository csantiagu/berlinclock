<html>
<head>
<style>
div {
	border: 2px solid;
	border-radius: 25px;
	display: inline-block;
}

.Y {
	background-color: yellow;
	text-align: center
}

.R {
	background-color: red;
	text-align: center
}

.O {
	background-color: white;
	text-align: center
}
</style>
<script type="text/javascript"
	src="http://code.jquery.com/jquery-latest.min.js"></script>
<script>
	$(document).ready(function() {

		$("#show").click(function() {
			var request = $.ajax({
				url : "/BerlinClock/clock",
				data : {
					time : $("#txtTime").val()
				}
			});
			request.done(function(msg) {
				if (msg != null && msg != "") {
					var lights = msg.split(" ");
					setFirstRowColor(lights[0]);
					setMultiRowsColor("hours1", lights[1]);
					setMultiRowsColor("hours2", lights[2]);
					setMultiRowsColor("mins1", lights[3]);
					setMultiRowsColor("mins2", lights[4]);

				} else {
					alert("Invalid Time.");
				}
			});
			request.fail(function(jqXHR, textStatus) {
				alert("Request failed: " + textStatus);
			});
		});

	});
	var setFirstRowColor = function(colors) {
		$("[name=seconds]").removeClass().addClass(colors).html(colors);
	};

	var setMultiRowsColor = function(obj, colors) {
		for (var i = 0; i < colors.length; i++) {
			$($("[name=" + obj + "]")[i]).removeClass().addClass(
					colors.charAt(i)).html(colors.charAt(i));
		}
	};
</script>

</head>
<body style="text-align: center;">
	<h2>Berlin Clock</h2>
	<input type="text" id="txtTime" autocomplete="on" />
	<input type="button" id="show" value="show">
	<br />
	<br />
	<div style="width: 550px" name="seconds">&nbsp;</div>
	</br>
	</br>
	<div style="width: 130px" name="hours1">&nbsp;</div>
	<div style="width: 130px" name="hours1">&nbsp;</div>
	<div style="width: 130px" name="hours1">&nbsp;</div>
	<div style="width: 130px" name="hours1">&nbsp;</div>
	<br /></br>
	<div style="width: 130px" name="hours2">&nbsp;</div>
	<div style="width: 130px" name="hours2">&nbsp;</div>
	<div style="width: 130px" name="hours2">&nbsp;</div>
	<div style="width: 130px" name="hours2">&nbsp;</div>
	<br /></br>
	<div style="width: 41px" name="mins1">&nbsp;</div>
	<div style="width: 41px" name="mins1">&nbsp;</div>
	<div style="width: 41px" name="mins1">&nbsp;</div>
	<div style="width: 41px" name="mins1">&nbsp;</div>
	<div style="width: 41px" name="mins1">&nbsp;</div>
	<div style="width: 41px" name="mins1">&nbsp;</div>
	<div style="width: 41px" name="mins1">&nbsp;</div>
	<div style="width: 41px" name="mins1">&nbsp;</div>
	<div style="width: 41px" name="mins1">&nbsp;</div>
	<div style="width: 41px" name="mins1">&nbsp;</div>
	<div style="width: 41px" name="mins1">&nbsp;</div>
	<br /></br>
	<div style="width: 130px" name="mins2">&nbsp;</div>
	<div style="width: 130px" name="mins2">&nbsp;</div>
	<div style="width: 130px" name="mins2">&nbsp;</div>
	<div style="width: 130px" name="mins2">&nbsp;</div>

</body>
</html>
