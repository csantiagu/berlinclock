package com.cas.controller;

import java.net.URLDecoder;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cas.pojo.BerlinClock;
import com.cas.pojo.BerlinClockImpl;

@Controller
public class BaseController {

	@RequestMapping(value = "/clock", method = RequestMethod.GET, headers = "Accept= */*")
	public @ResponseBody
	String getLocation(@RequestParam("time") String time) throws Exception {
		
		BerlinClock berlinClock = new BerlinClockImpl();

		return berlinClock.getBerlinClockString(time);
	}

}
