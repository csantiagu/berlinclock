package com.cas.pojo;

import java.util.List;

/**
 * User: santiaguc
 */

public interface BerlinClock {
 public String getBerlinClockString(String time);
}
