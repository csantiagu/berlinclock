package com.cas.pojo;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * User: santiaguc
 */

public class TimeValidator {

    Pattern pattern;
    Matcher matcher;
    private final String TIME_PATTERN =
            "([01]?[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]";

    public TimeValidator() {
        pattern = Pattern.compile(TIME_PATTERN);
    }

    public boolean validateTime(String time) {

        boolean validTime = false;

        if(time != null){

            if(exceptionalCase(time)){
                validTime = true;
            }
            else{
                matcher = pattern.matcher(time);
                validTime = matcher.matches();
            }

        }
        return validTime;
    }

    protected boolean exceptionalCase(String time)
    {
        if(time != null && time.equals("24:00:00")){
            return true;
        }
        else{
            return false;
        }

    }

}

