package com.cas.pojo;

import java.util.ArrayList;
import java.util.List;

/**
 * User: santiaguc
 */

public class BerlinClockImpl implements BerlinClock {

    protected List<String> getBerlinClock(String time) {
        List<String> berlinClock = null;
        List<Integer> rowLights = getRowLightNumbers(time);

        if(rowLights != null){
            berlinClock = renderLights(rowLights);
        }

        return berlinClock;
    }

    @Override
    public String getBerlinClockString(String time){

        String sberlinClock = null;
        List<String> berlinClock = getBerlinClock(time);

        if(berlinClock != null){
            StringBuilder sbBerlinClock = new StringBuilder();
            for(String rows:berlinClock){
                sbBerlinClock.append(rows).append(" ");
            }
            sberlinClock = sbBerlinClock.substring(0,sbBerlinClock.length()-1);
        }
        return sberlinClock;
    }

    protected List<Integer> getRowLightNumbers(String time) {

        List<Integer>rowLights = null;
        TimeValidator timeValidator = new TimeValidator();
        if(timeValidator.validateTime(time)){
            String[] timeArray = time.split(":");
            rowLights = new ArrayList<Integer>(5);

            // Seconds Row
            rowLights.add(Integer.parseInt(timeArray[2]) % 2);
            // Hours Row 1
            rowLights.add(Integer.parseInt(timeArray[0]) / 5);
            //Hours Row 2
            rowLights.add(Integer.parseInt(timeArray[0]) % 5);
            //Minutes Row 1
            rowLights.add(Integer.parseInt(timeArray[1]) / 5);
            //Minutes Row 2
            rowLights.add(Integer.parseInt(timeArray[1]) % 5);

        }

        return rowLights;
    }

    protected List<String> renderLights(List<Integer> lights) {
        List<String> renderedLights = null;
        if(lights != null){
            renderedLights = new ArrayList<String>(5);

            renderedLights.add(renderSeconds(lights.get(0)));

            renderedLights.add(renderHours(lights.get(1)));
            renderedLights.add(renderHours(lights.get(2)));

            renderedLights.add(renderMinsFirstRow(lights.get(3)));
            renderedLights.add(renderMinsSecondRow(lights.get(4)));
        }
        return renderedLights;
    }

    protected String renderSeconds(int seconds)
    {
        return (seconds == 0)?"Y":"O";
    }

    protected String renderHours(int hours)
    {
        return renderLights(hours,"R","OOOO");
    }

    protected String renderMinsFirstRow(int mins)
    {
        return renderLights(mins, "Y", "OOOOOOOOOOO").replaceAll("YYY", "YYR");
    }

    protected String renderMinsSecondRow(int mins)
    {
        return renderLights(mins, "Y", "OOOO");
    }

    private String renderLights(int time, String pattern, String lights){

        StringBuilder lightsOnOff = new StringBuilder(lights);
        for(int i=0; i<time; i++){
            lightsOnOff.replace(i,i+1,pattern);
        }

        return lightsOnOff.toString();
    }
}
